<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class UserTableSeeder extends Seeder
{
    public function run()
    {
        User::factory()->create();
    }
}