<?php

use App\Http\Controllers\PingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('ping', [PingController::class, 'ping']);

Route::name('auth')->group(base_path('routes/api/auth_route.php'));
Route::name('users')->group(base_path('routes/api/user_route.php'));
Route::name('finance')->group(base_path('routes/api/finance_account_route.php'));
