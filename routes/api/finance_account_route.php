<?php

use App\Http\Controllers\Finance\FinancialAccountController;
use App\Http\Controllers\Finance\FinancialTransactionController;
use Illuminate\Support\Facades\Route;

Route::resource('financial-accounts', FinancialAccountController::class)->middleware('auth:api');
Route::resource('financial-accounts/{financial_account}/financial-transactions',
    FinancialTransactionController::class)->middleware('auth:api');