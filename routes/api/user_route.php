<?php

use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;

Route::resource('users', UserController::class)
     ->middleware('auth:api')
     ->except('store');

Route::post('register', [UserController::class, 'register']);