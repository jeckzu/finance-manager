<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class FinancialAccount extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'description', 'user_id'
    ];
}