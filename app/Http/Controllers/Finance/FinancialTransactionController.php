<?php
declare(strict_types=1);

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use App\Repositories\Finance\FinancialTransactionRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class FinancialTransactionController extends Controller
{
    protected $routeName = 'financial_transaction';

    /**
     * FinancialTransactionController constructor.
     *
     * @param FinancialTransactionRepository $repository
     */
    public function __construct(FinancialTransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request): JsonResponse
    {
        $params = $request->route()->parameters;
        $financialAccount = $params['financial_account'];

        $result = $this->repository->findByFinancialAccount($financialAccount);

        return response()->json($result);
    }

    public function store(Request $request)
    {
        $params = $request->route()->parameters;
        $financialAccount = $params['financial_account'];

        $result = $this->repository->create(
            array_merge(
                [
                    'financial_account_id' => $financialAccount,
                    'user_id' => auth()->user()->id
                ],
                $request->all()
            )
        );

        return response()->json($result);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function update(Request $request)
    {
        $params = $request->route()->parameters;
        $id = $params['financial_transaction'];

        $result = $this->repository->update(
            array_merge(['id' => $id],
                $request->all())
        );

        return response()->json($result);
    }
}