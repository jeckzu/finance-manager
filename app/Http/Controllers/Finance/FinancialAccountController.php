<?php
declare(strict_types=1);

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use App\Repositories\Finance\FinancialAccountRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class FinancialAccountController extends Controller
{
    /**
     * @var string 
     */
    protected $routeName = 'financial_account';

    /**
     * FinanceAccountController constructor.
     *
     * @param FinancialAccountRepository $repository
     */
    public function __construct(FinancialAccountRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $result = $this->repository->create(
            array_merge(
                ['user_id' => auth()->user()->id],
                $request->all()
            )
        );

        return response()->json($result);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function update(Request $request)
    {
        $id = $request->route('financial_account');
        $result = $this->repository->update(
            array_merge(['id' => $id],
                $request->all())
        );

        return response()->json($result);
    }
}