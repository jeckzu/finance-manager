<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
interface RendererInterface
{
    public function render(Request $request, $data, array $options = []): Responsable;
}