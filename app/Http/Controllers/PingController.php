<?php
declare(strict_types=1);

namespace App\Http\Controllers;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class PingController extends Controller
{
    public function ping()
    {
        return response()->json(
            [
                'data'      => 'pong',
                'client_ip' => request()->ip(),
            ]
        );
    }
}