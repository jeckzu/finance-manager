<?php
declare(strict_types=1);

namespace App\Http\DataTransfer;

use Illuminate\Support\Facades\Hash;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class User
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $password;

    /**
     * User constructor.
     *
     * @param string $username
     * @param string $email
     * @param string $name
     * @param string $password
     */
    public function __construct(string $username, string $email, string $name, string $password)
    {
        $this->username = $username;
        $this->email = $email;
        $this->name = $name;
        $this->password = Hash::make($password);
    }

    public function toArray(): array
    {
        return [
            'username' => $this->username,
            'name'     => $this->name,
            'email'    => $this->email,
            'password' => $this->password
        ];
    }
}