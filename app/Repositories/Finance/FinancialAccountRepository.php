<?php
declare(strict_types=1);

namespace App\Repositories\Finance;

use App\Models\FinancialAccount;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class FinancialAccountRepository implements RepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return FinancialAccount::all();
    }

    /**
     * @param string $id
     *
     * @return Model
     */
    public function find(string $id): Model
    {
        return FinancialAccount::findOrFail($id);
    }

    /**
     * @param array $payload
     *
     * @return Model
     */
    public function update(array $payload): Model
    {
        $financialAccount = $this->find($payload['id']);
        $financialAccount->type = $payload['type'];
        $financialAccount->description = $payload['description'];

        $financialAccount->save();

        return $financialAccount;
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function delete(string $id): bool
    {
        return FinancialAccount::findOrFail($id)->delete();
    }

    public function create(array $payload): FinancialAccount
    {
        $financialAccount = new FinancialAccount($payload);
        $financialAccount->id = Uuid::uuid4();
        $financialAccount->save();

        return $financialAccount;
    }
}