<?php
declare(strict_types=1);

namespace App\Repositories\Finance;

use App\Models\FinancialTransaction;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class FinancialTransactionRepository implements RepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return FinancialTransaction::all();
    }

    /**
     * @param string $id
     *
     * @return Model
     */
    public function find(string $id): Model
    {
        return FinancialTransaction::findOrFail($id);
    }

    /**
     * @param string $financialAccount
     *
     * @return mixed
     */
    public function findByFinancialAccount(string $financialAccount)
    {
        return FinancialTransaction::where(['financial_account_id' => $financialAccount])->get();
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function delete(string $id): bool
    {
        return FinancialTransaction::findOrFail($id)->delete();
    }

    public function create(array $payload): FinancialTransaction
    {
        $data = new FinancialTransaction($payload);
        $data->id = Uuid::uuid4();
        $data->save();

        return $data;
    }

    /**
     * @param array $payload
     *
     * @return Model
     */
    public function update(array $payload): Model
    {
        $data = $this->find($payload['id']);
        $data->title = $payload['title'];
        $data->description = $payload['description'];
        $data->amount = $payload['amount'];

        $data->save();

        return $data;
    }
}