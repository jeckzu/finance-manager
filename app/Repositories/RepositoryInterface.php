<?php
declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
interface RepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param string $id
     *
     * @return Model
     */
    public function find(string $id): Model;

    /**
     * @param string $id
     *
     * @return bool
     */
    public function delete(string $id): bool;
}