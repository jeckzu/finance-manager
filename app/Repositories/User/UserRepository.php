<?php
declare(strict_types=1);

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

/**
 * @author  Aji GP. <ajigumelarp@gmail.com>
 */
class UserRepository implements RepositoryInterface
{
    /**
     * @return User[]|Collection
     */
    public function all(): Collection
    {
        return User::all();
    }

    /**
     * @param string $userId
     *
     * @return Model
     */
    public function find(string $userId): Model
    {
        return User::where('id', $userId)->first();
    }

    /**
     * @param array $payload
     *
     * @return User
     */
    public function create(array $payload)
    {
        $user = new User($payload);
        $user->id = Uuid::uuid4();
        $user->save();

        return $user;
    }

    /**
     * @param $user
     *
     * @return bool
     */
    public function update($user): bool
    {
        return $user->save();
    }

    /**
     * @param string $userId
     *
     * @return bool
     */
    public function delete(string $userId): bool
    {
        return User::where('id', $userId)->delete();
    }
}